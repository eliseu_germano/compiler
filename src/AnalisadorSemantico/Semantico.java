package AnalisadorSemantico;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import AnalisadorLexico.DadosLexico;
import AnalisadorLexico.Simbolo;

public class Semantico {
	public static int x = 0;
	public static boolean primeiroPrint = true;
	
	public static Simbolo analisador(Simbolo[] simbolo, int regra) {

		switch (regra) {
			case 5:
				imprimir("\n\n");
				return simbolo[5];
			case 6:
				simbolo[0].setTipo(simbolo[1].getTipo());
				imprimir(getTipo(simbolo[1].getTipo()) + " " + simbolo[0].getLexema() + ";");
				return simbolo[5];
			case 7:
				simbolo[5].setTipo(simbolo[0].getTipo());
				return simbolo[5];
			case 8:
				simbolo[5].setTipo(simbolo[0].getTipo());
				return simbolo[5];
			case 9:
				simbolo[5].setTipo(simbolo[0].getTipo());
				return simbolo[5];
			case 11:
				if (!"".equals(simbolo[1].getTipo())) {
					switch (simbolo[1].getTipo()) {
					case "literal":
						imprimir("scanf(“%s”, " + simbolo[1].getLexema() + ");");
						return simbolo[5];
					case "inteiro":
						imprimir("scanf(“%d”, &" + simbolo[1].getLexema() + ");");
						return simbolo[5];
					case "real":
						imprimir("scanf(“%lf”, &" + simbolo[1].getLexema() + ");");
						return simbolo[5];
					}
	
				} else {
					System.out.println("Erro: Variável não declarada 11");
				}
				break;
			case 12:
				imprimir("printf(" + simbolo[1].getLexema() + ");");
				return simbolo[5];
			case 13:
				simbolo[5].setTipo(simbolo[0].getTipo());
				simbolo[5].setLexema(simbolo[0].getLexema());
				simbolo[5].setId(simbolo[0].getId());
				return simbolo[5];
			case 14:
				simbolo[5].setTipo(simbolo[0].getTipo());
				simbolo[5].setLexema(simbolo[0].getLexema());
				simbolo[5].setId(simbolo[0].getId());
				return simbolo[5];
			case 15:
				if (!"".equals(simbolo[0].getTipo())) {
					simbolo[5].setTipo(simbolo[0].getTipo());
					simbolo[5].setLexema(simbolo[0].getLexema());
					simbolo[5].setId(simbolo[0].getId());
					return simbolo[5];
				} else
					System.out.println("Erro: Variável não declarada [Regra 15]");
				break;
			case 17:
				if (!"".equals(simbolo[0].getTipo())) {
					if (simbolo[0].getTipo().equals(simbolo[2].getTipo())) {
						imprimir(simbolo[0].getLexema() + " =" + simbolo[1].getTipo() + " " + simbolo[2].getLexema()+";");
						return simbolo[5];
					} else
						System.out.println("Erro: tipos diferentes para atribuição [Regra 17]");
				} else
					System.out.println("Erro: Variável não declarada [Regra 17]");
				break;
			case 18:
				if (simbolo[0].getTipo().equals(simbolo[2].getTipo()) && !"literal".equals(simbolo[0].getTipo())) {
					simbolo[5].setLexema("T");
					simbolo[5].setLexema(simbolo[5].getLexema()+x);
					imprimir(simbolo[5].getLexema() + " = " + simbolo[0].getLexema() + " " + simbolo[1].getLexema() + " " + simbolo[2].getLexema() +";");
					x++;
					simbolo[5].setTipo(simbolo[0].getTipo());
					return simbolo[5];
				} else
					System.out.println("Erro: Operandos com tipos incompatíveis [Regra 18]");
				break;
			case 19:
				simbolo[5].setTipo(simbolo[0].getTipo());
				simbolo[5].setLexema(simbolo[0].getLexema());
				simbolo[5].setId(simbolo[0].getId());
				return simbolo[5];
			case 20:
				if (DadosLexico.simbolos.containsKey(simbolo[0].getLexema())) {
					simbolo[5].setTipo(simbolo[0].getTipo());
					simbolo[5].setLexema(simbolo[0].getLexema());
					simbolo[5].setId(simbolo[0].getId());
				} else
					System.out.println("Erro: Variável não declarada [Regra 20]");
				return simbolo[5];
			case 21:
				simbolo[5].setTipo(simbolo[0].getTipo());
				simbolo[5].setLexema(simbolo[0].getLexema());
				simbolo[5].setId(simbolo[0].getId());
				return simbolo[5];
			case 23:
				imprimir("}");
				return simbolo[5];
			case 24:
				imprimir("if(" + simbolo[2].getLexema() + ") {");
				return simbolo[5];
			case 25:
				if (simbolo[0].getTipo().equals(simbolo[2].getTipo())) {
					simbolo[5].setLexema("T");
					simbolo[5].setLexema(simbolo[5].getLexema()+x);
					imprimir(simbolo[5].getLexema() + " = " + simbolo[0].getLexema() + " " + simbolo[1].getLexema() + " " + simbolo[2].getLexema() +";");
					x++;
					return simbolo[5];
				} else
					System.out.println("Erro: Operandos com tipos incompatíveis [Regra 25]");
				break;
			default:
				break;
		}
		return simbolo[5];
	}
	
	private static String getTipo(String tipo){
		switch (tipo) {
			case "inteiro":
				return "int";
			case "real":
				return "double";
			default:
				return tipo;
		}
	}

	public static void imprimir(String arg) {
		File file1 = new File("/home/eliseu/Documents/Compiladores/TrabalhoCompiladores/src/AnalisadorSintatico/saida.txt");
		File file2 = new File("/home/eliseu/Documents/Compiladores/TrabalhoCompiladores/src/AnalisadorSintatico/PROGRAMA.c");
		try {
			FileWriter fw1 = new FileWriter(file1, true);
			FileWriter fw2 = new FileWriter(file2, true);
			
			BufferedWriter bw1 = new BufferedWriter(fw1);
			BufferedWriter bw2 = new BufferedWriter(fw2);
			
			
			bw1.write(arg);
			bw1.newLine();
			
			if(primeiroPrint){
				String aux = "#include<stdio.h>\n\ntypedef char literal[256];\nvoid main(void)\n{\n/*----Variaveis temporarias----*/\nint T0;\nint T1;\nint T2;\nint T3;\nint T4;\n/*------------------------------*/";
				bw2.write(aux);
				bw2.newLine();
				
				bw2.write(arg);
				bw2.newLine();
				primeiroPrint = false;
			} else {
				bw2.write(arg);
				bw2.newLine();
			}
			
			bw1.close();
			fw1.close();
			bw2.close();
			fw2.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
