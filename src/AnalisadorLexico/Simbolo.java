package AnalisadorLexico;

public class Simbolo {
	
	private String id;
	private String tipo;
	private String lexema;
	private int sLinha;
	private String producao;
	
	public Simbolo(String id, String tipo, String lexema) {
		super();
		this.id = id;
		this.tipo = tipo;
		this.lexema = lexema;
	}
	
	public Simbolo(){
		
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getLexema() {
		return lexema;
	}

	public void setLexema(String lexema) {
		this.lexema = lexema;
	}
	
	public int getsLinha() {
		return sLinha;
	}
	
	public void setsLinha(int sLinha) {
		this.sLinha = sLinha;
	}
	
	public String getProducao() {
		return producao;
	}
	
	public void setProducao(String producao) {
		this.producao = producao;
	}
}
