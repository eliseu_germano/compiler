package AnalisadorLexico;

import java.util.Enumeration;

public class Lexico {
	
	private DadosLexico dados;
	private static int POSICAO_FONTE=0;
	public static String lexema = "";
	private static int erroLinha=1;
	private static int erroColuna=0;
	private static boolean flag=false;
	
	public Lexico(DadosLexico dados){
		this.dados = dados;
	}
	
	public int analisador(){
		int estado = 0, estadoAnterior=0;
		char caracter;
		String fonte = dados.getFonte();
		lexema = "";
		
		if(POSICAO_FONTE == fonte.length()-1){
			return DadosLexico.TOKEN_EOF;
		} else {
			do {
				caracter = fonte.charAt(POSICAO_FONTE);
				estadoAnterior = estado;
				estado = dados.getTransicao(estado, dados.mapa_caracter_coluna(caracter));
				POSICAO_FONTE++;
				
				// Usado para identificar linha e coluna de erros lexicos.
				if (flag!= true){
					if(caracter == '\n'){
						erroLinha++;
						erroColuna=0;
					} else {
						erroColuna++;
					}
				} else {
					flag=false;
				}
				
				if(estado != -1)
					lexema += caracter;
			}while(estado != -1);
		}
		
		POSICAO_FONTE--; // garante que simbolos '\n' em um literal seja diferenciados dos '\n' do codigo-fonte.
		flag=true; // usado para auxiliar na identificacao do numero de uma linha com erro.
		
		return dados.mapa_estado_token(estadoAnterior);
	}

	public static int getErroLinha() {
		return erroLinha;
	}

	public static int getErroColuna() {
		return erroColuna;
	}
	
	public static Simbolo proximoSimbolo(int token) {

		switch (token) {
			case DadosLexico.TOKEN_COMENTARIO:
				return new Simbolo("comentario", "", Lexico.lexema);
			case DadosLexico.TOKEN_LITERAL:
				return new Simbolo("literal", "literal", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_MAIOR:
				return new Simbolo("OPR", "OPR_MAIOR", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_IGUAL:
				return new Simbolo("OPR", "OPR_IGUAL", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_MENOR:
				return new Simbolo("OPR", "OPR_MENOR", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_MAIOR_IGUAL:
				return new Simbolo("OPR", "OPR_MAIOR_IGUAL", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_MENOR_IGUAL:
				return new Simbolo("OPR", "OPR_MENOR_IGUAL", Lexico.lexema);
			case DadosLexico.TOKEN_OPR_DIFERENTE:
				return new Simbolo("OPR", "OPR_DIFERENTE", Lexico.lexema);
			case DadosLexico.TOKEN_OPM_SOMA:
				return new Simbolo("OPM", "OPM_SOMA", Lexico.lexema);
			case DadosLexico.TOKEN_OPM_SUBTRACAO:
				return new Simbolo("OPM", "OPM_SUBTRACAO", Lexico.lexema);
			case DadosLexico.TOKEN_OPM_MULTIPLICACAO:
				return new Simbolo("OPM", "OPM_MULTIPLICACAO", Lexico.lexema);
			case DadosLexico.TOKEN_OPM_DIVISAO:
				return new Simbolo("OPM", "OPM_DIVISAO", Lexico.lexema);
			case DadosLexico.TOKEN_ATRIBUICAO:
				return new Simbolo("RCB", "", Lexico.lexema);
			case DadosLexico.TOKEN_IDENTIFICADOR:
				//System.out.println("+-+-+-+-+-+-+-+-+-+-+-+ \n"+removeEspaco(Lexico.lexema));
				//printHash();
				if(DadosLexico.simbolos.containsKey(removeEspaco(Lexico.lexema))){
					//System.out.println("+-+-+-+-+-+-+-+-+-+-+-+ sim \n\n");
					return DadosLexico.simbolos.get(removeEspaco(Lexico.lexema));
				} else {
					Simbolo simbolo = new Simbolo("ID", "", removeEspaco(Lexico.lexema));
					DadosLexico.simbolos.put(removeEspaco(Lexico.lexema), simbolo);
					return simbolo;
				}
			case DadosLexico.TOKEN_INTEIRO:
				return new Simbolo("num", "inteiro", Lexico.lexema);
			case DadosLexico.TOKEN_INTEIRO_EXPOENTE:
				return new Simbolo("num", "Expoente", Lexico.lexema);
			case DadosLexico.TOKEN_REAL:
				return new Simbolo("num", "real", Lexico.lexema);
			case DadosLexico.TOKEN_ABRE_PARENTESE:
				return new Simbolo("AB_P", "", Lexico.lexema);
			case DadosLexico.TOKEN_FECHA_PARENTESE:
				return new Simbolo("FC_P", "", Lexico.lexema);
			case DadosLexico.TOKEN_PONTO_VIRGULA:
				return new Simbolo("PT_V", "", Lexico.lexema);
			default:
				return null;
		}
	}
	
	public static void printHash(){
		System.out.println("\n\n\n\n\n\n\n HashTable ");
		Enumeration keys = DadosLexico.simbolos.keys();
		while(keys.hasMoreElements()){
			String key = (String) keys.nextElement();
			System.out.println(key + " : " + DadosLexico.simbolos.get(key));
		}
	}
	
	public static String removeEspaco(String string){
		string = string.replaceAll("\n", "");
		string = string.replaceAll(" ", "");
		return string;
	}
}

