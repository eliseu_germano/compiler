package AnalisadorLexico;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Hashtable;

public class DadosLexico {
	
	public static final int TOKEN_COMENTARIO = 2;
	public static final int TOKEN_LITERAL = 4;
	public static final int TOKEN_OPR_MAIOR = 5;
	public static final int TOKEN_OPR_IGUAL = 7;
	public static final int TOKEN_OPR_MENOR = 8;
	public static final int TOKEN_OPR_MAIOR_IGUAL = 6;
	public static final int TOKEN_OPR_MENOR_IGUAL = 10;
	public static final int TOKEN_OPR_DIFERENTE = 11;
	public static final int TOKEN_OPM_SOMA = 12;
	public static final int TOKEN_OPM_SUBTRACAO = 13;
	public static final int TOKEN_OPM_MULTIPLICACAO = 14;
	public static final int TOKEN_OPM_DIVISAO = 15;
	public static final int TOKEN_ATRIBUICAO = 9;
	public static final int TOKEN_EOF = 26;
	public static final int TOKEN_ERRO = -1; //
	public static final int TOKEN_IDENTIFICADOR = 19;
	public static final int TOKEN_INTEIRO = 20;
	public static final int TOKEN_INTEIRO_EXPOENTE = 25;
	public static final int TOKEN_REAL = 22;
	public static final int TOKEN_REAL_EXPOENTE = 25;
	public static final int TOKEN_ABRE_PARENTESE = 16; //
	public static final int TOKEN_FECHA_PARENTESE = 17; //
	public static final int TOKEN_PONTO_VIRGULA = 18; //
		
	static final int ESPACO = 0;
	static final int BARRA_N = 1;
	static final int ABRE_CHAVE = 2;
	static final int FECHA_CHAVE = 3;
	static final int ASPAS = 4;
	static final int MAIOR = 5;
	static final int IGUAL = 6;
	static final int MENOR = 7;
	static final int SOMA = 8;
	static final int SUBTRACAO = 9;
	static final int MULTIPLICACAO = 10;
	static final int DIVISAO = 11;
	static final int ABRE_P = 12;
	static final int FECHA_P = 13;
	static final int PONTO_VIRG = 14;
	static final int LITERAL = 15;
	static final int DIGITO = 16;
	static final int PONTO = 17;
	static final int EXPOENTE = 18;
	static final int OUTRO = 20;
	
	public static Hashtable <String, Simbolo> simbolos = new Hashtable<>();
	
	private String fonte;
	private String path;
									// 0   1   2   3   4   5   6   7   8   9  10  11  12  13  14  15  16  17  18  19   20								    
									//esp  \n  {   }   "   >   =   <   +   -   *   /   (   )   ;   L   D   .   E  EOF
	private int[][] tabTransicao = { { 0,  0,  1, -1,  3,  5,  7,  8, 12, 13, 14, 15, 16, 17, 18, 19, 20, -1, -1, 26, -1 }, // 0
									{  1,  1,  1,  2,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1,  1 }, // 1
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 2
									{  3,  3,  3,  3,  4,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3,  3 }, // 3
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 4
									{ -1, -1, -1, -1, -1, -1,  6, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 5
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 6
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 7
									{ -1, -1, -1, -1, -1, 11, 10, -1, -1,  9, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 8
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 9
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 10
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 11
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 12
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 13
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 14
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 15
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 16
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 17
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }, // 18
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 19, 19, -1, -1, -1, 19 }, // 19
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 20, 21, 23, -1, -1 }, // 20
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, -1, -1, -1 }, // 21
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 22, -1, 23, -1, -1 }, // 22
									{ -1, -1, -1, -1, -1, -1, -1, -1, 24, 24, -1, -1, -1, -1, -1, -1, 25, -1, -1, -1, -1 }, // 23
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 25, -1, -1, -1, -1 }, // 24
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, 25, -1, -1, -1, -1 }, // 25
									{ -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1, -1 }  // 26
									};
	
	public DadosLexico(String path){
		this.path = path;
		initHashTable();
		entradaDados(path);
	}
	
	private void initHashTable(){
		simbolos.put("inicio", new Simbolo("inicio", "", "inicio"));
		simbolos.put("varinicio", new Simbolo("varinicio", "", "varinicio"));
		simbolos.put("varfim", new Simbolo("varfim", "", "varfim"));
		simbolos.put("escreva", new Simbolo("escreva", "", "escreva"));
		simbolos.put("leia", new Simbolo("leia", "", "leia"));
		simbolos.put("se", new Simbolo("se", "", "se"));
		simbolos.put("entao", new Simbolo("entao", "", "entao"));
		simbolos.put("fimse", new Simbolo("fimse", "", "fimse"));
		simbolos.put("fim", new Simbolo("fim", "", "fim"));
		simbolos.put("inteiro", new Simbolo("inteiro", "inteiro", "inteiro"));
		simbolos.put("real", new Simbolo("real", "real", "real"));
		simbolos.put("literal", new Simbolo("literal", "literal", "literal"));
	}
	
	public int mapa_caracter_coluna(char c){
		if(c==' ')return ESPACO;
		if(c=='\n')return BARRA_N;
		if(c=='{')return ABRE_CHAVE;
		if(c=='}')return FECHA_CHAVE;
		if(c=='"')return ASPAS;
		if(c=='>')return MAIOR;
		if(c=='=')return IGUAL;
		if(c=='<')return MENOR;
		if(c=='+')return SOMA;
		if(c=='-')return SUBTRACAO;
		if(c=='*')return MULTIPLICACAO;
		if(c=='/')return DIVISAO;
		if(c=='(')return ABRE_P;
		if(c==')')return FECHA_P;
		if(c==';')return PONTO_VIRG;
		if(Character.isLetter(c))return LITERAL;
		if(Character.isDigit(c))return DIGITO;
		if(c=='.')return PONTO;
		if(c=='E')return EXPOENTE;
		return OUTRO;
	}
	
	public int mapa_estado_token(int estado) {
		
		if(estado == 2) return TOKEN_COMENTARIO;
		if(estado == 4) return TOKEN_LITERAL;
		if(estado == 5) return TOKEN_OPR_MAIOR;
		if(estado == 7) return TOKEN_OPR_IGUAL;
		if(estado == 8) return TOKEN_OPR_MENOR;
		if(estado == 6) return TOKEN_OPR_MAIOR_IGUAL;
		if(estado == 10) return TOKEN_OPR_MENOR_IGUAL;
		if(estado == 11) return TOKEN_OPR_DIFERENTE;
		if(estado == 12) return TOKEN_OPM_SOMA;
		if(estado == 13) return TOKEN_OPM_SUBTRACAO;
		if(estado == 14) return TOKEN_OPM_MULTIPLICACAO;
		if(estado == 15) return TOKEN_OPM_DIVISAO;
		if(estado == 9) return TOKEN_ATRIBUICAO;
		if(estado == 26) return TOKEN_EOF;
		if(estado == -1) return TOKEN_ERRO;
		if(estado == 19) return TOKEN_IDENTIFICADOR;
		if(estado == 20) return TOKEN_INTEIRO;
		if(estado == 25) return TOKEN_INTEIRO_EXPOENTE;
		if(estado == 22) return TOKEN_REAL;
		if(estado == 25) return TOKEN_REAL_EXPOENTE;
		if(estado == 16) return TOKEN_ABRE_PARENTESE;
		if(estado == 17) return TOKEN_FECHA_PARENTESE;
		if(estado == 18) return TOKEN_PONTO_VIRGULA;
		return TOKEN_ERRO;
	}
	
	private void entradaDados(String path){
		String linha = "";
		String fonte = "";

		try {
			FileInputStream file = new FileInputStream(path+"fonte_entrada.txt");
			BufferedReader buffer = new BufferedReader(new InputStreamReader(file));

			linha = buffer.readLine();
			fonte += linha + "\n";

			while (linha != null) {
				linha = buffer.readLine();
				if(linha != null) fonte += linha + "\n";
			}
			buffer.close();

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		this.fonte = fonte;
	}
	
	public int getTransicao(int i, int j) {
		return tabTransicao[i][j];
	}
	
	public String getFonte() {
		return fonte;
	}
}
