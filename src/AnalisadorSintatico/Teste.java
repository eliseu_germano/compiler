package AnalisadorSintatico;

import AnalisadorLexico.DadosLexico;
import AnalisadorLexico.Lexico;

public class Teste {

	public static void main(String[] args) {
		DadosLexico dados = new DadosLexico("/home/eliseu/Documents/Compiladores/TrabalhoCompiladores/src/AnalisadorSintatico/");
		Lexico lexico = new Lexico(dados);
		Sintatico.analisador(dados.getFonte(), lexico);
	}

}
