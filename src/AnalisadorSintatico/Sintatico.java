package AnalisadorSintatico;

import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;
import java.util.Stack;

import AnalisadorLexico.DadosLexico;
import AnalisadorLexico.Lexico;
import AnalisadorLexico.Simbolo;
import AnalisadorSemantico.Semantico;

public class Sintatico {
	private static boolean erro = false;
	
	public static void analisador(String texto_fonte, Lexico lexico) {
		int token;
		Simbolo estadoTopoPilha;
		String valor = "", acao = "";
		Stack<Simbolo> pilha = new Stack<Simbolo>();
		Simbolo conjuntoSimbolos[] = new Simbolo[6];
		int coluna = 0, desempilhar = 0, j;
		
		// Faz ip apontar para o primeiro simbolo
		token = lexico.analisador();
		Simbolo inicio = new Simbolo();
		inicio.setsLinha(0);
		pilha.push(inicio); // adiciona 0 no topo da pilha

		if (token == DadosLexico.TOKEN_ERRO) {
			System.out.println("Erro Lexico em (linha;coluna): (" + Lexico.getErroLinha() + ";" + Lexico.getErroColuna() + ")");
			DadosLexico.simbolos.put("ERRO", new Simbolo("ERRO", "", Lexico.lexema));
		}
		
		// Gera o objeto com as informacoes do simbolo (token, lexima, tipo)
		Simbolo simbolo = Lexico.proximoSimbolo(token);

		while (true) {
			Simbolo sLinha = new Simbolo();
			Simbolo producao = new Simbolo();
			Simbolo valorTabela = new Simbolo();
			
			estadoTopoPilha = pilha.peek(); // estado do topo da pilha
			String lexema = removeEspaco(simbolo.getLexema());
			coluna = getColunaSintatico(lexema, simbolo);
			
			System.out.println("\n================================================\n ");
			System.out.println("Lexema: " + lexema);
			System.out.println("TABELA_SINTATICA: (" + estadoTopoPilha.getsLinha() +"; " +coluna+ ")\n");
			
			if(coluna==-1){ erro=true; break; }
			valor = DadosSintatico.TABELA_SINTATICA[estadoTopoPilha.getsLinha()][coluna];
			acao = getAcao(valor);
			System.out.println("Valor na Tabela: " + valor);
			System.out.println("acao: " + acao);

			if (acao.equals("Empilhar")) {

				// empilha o simbolo apontado por ip e em seguida s' no topo da pilha
				
				sLinha.setsLinha(DadosSintatico.getValor(valor));
				pilha.push(simbolo);
				pilha.push(sLinha);
				System.out.println("\n => Empilha: simbolo e s'=" + sLinha.getsLinha());
				System.out.println("	=> Id: " + simbolo.getId());
				System.out.println("	=> Tipo: " + simbolo.getTipo());
				System.out.println("	=> Lexema: " + simbolo.getLexema());
				
				// avanca ip para o proximo simbolo de entrada
				token = lexico.analisador();
				System.out.println("Novo token: " + token);
				simbolo = Lexico.proximoSimbolo(token);
			} else if (acao.equals("Reduzir")) {
				sLinha.setsLinha(DadosSintatico.getValor(valor));
				desempilhar = 2 * (Integer.parseInt(DadosSintatico.PRODUCOES[sLinha.getsLinha() - 1][0]));
				j=1;
				for (int i = 0; i < desempilhar; i++) {
					if(i%2==1){
						Simbolo d = pilha.peek();
						conjuntoSimbolos[(desempilhar/2)-j] = d;
						j++;
					}
					pilha.pop();
				}

				estadoTopoPilha = pilha.peek();
				producao.setProducao(DadosSintatico.PRODUCOES[sLinha.getsLinha() - 1][1]);
				conjuntoSimbolos[5] = producao;
				Simbolo gerado = Semantico.analisador(conjuntoSimbolos, sLinha.getsLinha());
				pilha.push(gerado);
				
				System.out.println("producao: " + producao.getProducao());
				coluna = DadosSintatico.getTokenColuna(DadosSintatico.PRODUCOES[sLinha.getsLinha() - 1][1]);
				valor = DadosSintatico.TABELA_SINTATICA[estadoTopoPilha.getsLinha()][coluna];
				
				valorTabela.setsLinha(Integer.parseInt(removeEspaco(valor)));
				pilha.push(valorTabela);
				System.out.println("\n => Reduz: " + DadosSintatico.PRODUCOES[sLinha.getsLinha() - 1][2]);
				
			} else if (acao.equals("Aceitar")) {
				System.out.println("Aceito.");
				break;
			} else {
				System.out.println("Erro Sintatico.");
				break;
			}
		}
		if(erro) System.out.println("Erro Sintatico.");
	}

	public static int getColunaSintatico(String lexema, Simbolo simbolo){
		int coluna;
		if(lexema.equals(";"))
    		coluna = DadosSintatico.getTokenColuna(simbolo.getId());
    	else if(lexema.equals("literal"))
    		coluna = DadosSintatico.getTokenColuna("lit");
    	else if(lexema.charAt(0)=='"')
    		coluna = DadosSintatico.getTokenColuna("literal");
    	else if(lexema.charAt(0)=='(')
    		coluna = DadosSintatico.getTokenColuna("AB_P");
    	else if(lexema.charAt(0)==')')
    		coluna = DadosSintatico.getTokenColuna("FC_P");
    	else if(lexema.charAt(0)=='>' || lexema.equals("<="))
    		coluna = DadosSintatico.getTokenColuna("OPR");
    	else if(Character.isDigit(lexema.charAt(0)))
    		coluna = DadosSintatico.getTokenColuna("num");
    	else if(lexema.equals("<-"))
    		coluna = DadosSintatico.getTokenColuna("RCB");
    	else if(lexema.charAt(0)=='+' || lexema.charAt(0)=='-' || lexema.charAt(0)=='*' || lexema.charAt(0)=='/')
    		coluna = DadosSintatico.getTokenColuna("OPM");
    	else if(identificadores(lexema))
			coluna = DadosSintatico.getTokenColuna(simbolo.getId());
    	else if(lexema.charAt(0)=='$')
    		coluna = DadosSintatico.getTokenColuna("EOF");
    	else
    		coluna = DadosSintatico.getTokenColuna(simbolo.getLexema());
		
		return coluna;
	}

	public static String getAcao(String v) {
		if (v.charAt(0) == 's'){
			return "Empilhar";
		} else if (v.charAt(0) == 'r'){
			return "Reduzir";
		} else if (v.equals("ACC")){
			return "Aceitar";
		} else {
			return "Acao nao identificada.";
		}
	}
	
	public static String removeEspaco(String string){
		string = string.replaceAll("\n", "");
		string = string.replaceAll(" ", "");
		return string;
	}
	
	public static boolean identificadores(String id){
		
		if(id.length()==1 && DadosLexico.simbolos.containsKey(id)){
			// Usado para checar conteudo da tabela de simbolos
			/*
			System.out.println("\n\n\n\n\n\n\n HashTable "+ id);
			Enumeration keys = DadosLexico.simbolos.keys();
			while(keys.hasMoreElements()){
				String key = (String) keys.nextElement();
				System.out.println(key + " : " + DadosLexico.simbolos.get(key));
			}
			*/
			return true;
		}
		return false;
	}
}
